Weekly Updates: April 11th, 2022
********************************

**DAO Suite**

- **yield.vote**

  - Start FVT Stake-Weighted Voting Basic Pool. Chris S. 

- New ID art. Tech possibly ready. Chris S. 

**Alliances**

- Potential NFT project. 
  
  - Interested in Auction and submission and voting tools to choose art for IDs. 
  - Discussions will be ongoing for now. 

- In talks with potential investors.
- Nick speaking with 2 potential DAOs.
- Nick in discussion with Advisor Alex Kruger on potential NFT DAO project. 

**Communications & Marketing**

- Content

  - "What is FactoryDAO?" written piece. In final editing.
  - "Defining DAOs" written piece. In final editing.
  - Contribution piece. Nick.
  - What makes FactoryDAO Special? – possibly a community task or crowd sourced response.

- Nick being invited to speak at various talks.
- Call with Sovyrn community went well. 
- Season 0 

  – Getting processes ready till we execute a Season 1.
  - Metrics for us to determine when we are ready.
  - Preparation phase where we are getting ready. 

- Possible Seasons and Messages to Communicate:

  - Modular structures and DAO journey and integration.
  - NFT governance, how to communicate it, and getting projects to use.
  - We have exciting tools that are ahead of the space. How to communicate?

- Demonstrate that our tools are usable right now. Showcasing that they are working. 
- Thurs. Twitter Spaces with Midnite Movie Club
  
  - Matthew Lillard, Bill Whirity, & Dr. Nick. 
  - Discussing MMC mint and its Day 1 utility and a DAO powered by FactoryDAO.

- Twitter Spaces with Sketchy Ape Book Club
  
  - Finalize date.

- Migrate FVT IDs and gate the FVT Discord. 
- Intro videos for the DAO Suite - tailored videos for the audiences. 
- Announcements, images, & possible video for FVT Stake-Weighted Voting Basic Pool launch. 

**Community**

- Followup vote on FVT Citizen IDs price and decay rate. Being written. Matt. Editing by DAO leaders. 
- Create tasks in PropaganDAO. Active work anyone can do with bounties using NFTs. Vote.



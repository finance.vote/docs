Weekly Updates: February 28th, 2022
***********************************

This week the team worked and met asynchronously.  Work continues on The Programme, the new rebrand, the 6 week showcase, potential partnerships, and more. Stay tuned ;)

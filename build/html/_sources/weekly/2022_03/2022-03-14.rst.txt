Weekly Updates: March 14th, 2022
********************************

**Matrix of Responsibility: Showcase**

*{Task; Responsible}*

- Advertising via TG and Discords; Dom 
- Content Plan {2 weeks}; Matt, Ksenya, Nick
- Setup FactoryDAO TG, Announcement/chat; Ksenya
- Tweet Writing; Dom
- Twitter Content; Dom 
- Website Landing Page; Naomi
- Website; Naomi
- Weekly Community Update; Lizzl
- Community Recruitment; Ksenya
- SubDAO and Core Task Lists; DAO Leaders
- Put tasks on Dework; Matt
- Weekly Help Wanted Design; Bankole
- Audience / DAO specific decks; Nick, Naomi
- Creative Briefing Process; Nick, Naomi, Matt
- Build 1 pager templates for sales; Nick / matt
- Budgeting for miniDAOs; Robyn/Fred/new guy 
- Fund Community Treasuries; Matt, Ksenya, Naved
- Content Recording Process; Matt, Ksenya
- DAO Stats (Template); Nick, Lizzl
- Land payouts for work; Matt, Naved, Ksenya
- Governance Drop - Data Cleaning; Lizzl
- Governance Drop - Drop; Chris S.
- Graphic Design Deck Workflow; Naomi, Nick, Matt, Naved, Bankole, Chris C
- Plan Mint Party; Chris C.
- Construct Quiz; Chris C. 
- Build Sheet for quiz questions; Naved
- Collect 3 questions from each sessions; Matt, Ksenya, Naved, Nick, Chris
- Collect session forms week 1, 2; Ksenya
- Confirm Masser & Robyn for community call; Matt
- Confirm NFTA for reading and wrinting club; Matt
- Distribute Session forms week 1, 2; Ksenya
- FactoryDAO begins video / article; Nick
- Invite guests; Nick
- Programme Template; Bankole, Naomi
- Share format for announcements, airtable; Matt
- Sizzle video(s); Chris C. 
- Speak to potential Imagination Time host; Naved
- Discord Set up; Chris C.
- Gating FVT Discord; Chris C.
- Influence 2.0 Designs; Chris S., Naomi
- Influence 2.0 Launch; Chris S., Naomi
- Launching Basic Pool; Chris S.
- Mint Modifications {multi mint/reveal}; Chris S.
- Parliament ID Launch; Nick, Naomi, Chris
- Token economics spreadsheets; Nick 
- Plan Weekly votes {vote template}; Nick, Matt, Naved
- Set up weekly votes; Nick, Matt, Naved
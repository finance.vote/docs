Weekly Updates: May 16th, 2022
******************************

**DAO Suite**

- **influence.vote**

  - Submission/Realm address access for Ksenya.
  - Showcase and test net Factory DAO — message Martha.

- Multichain

  - Move to next chain & incentivized test.

    - Waiting for response from their marketing team.
    - First, incentivized test using launch.vote. 
    - Test net FACT token on new chain. Can purchase and then pushed to partner's own swap site and then become LP.
    - Next, launch yield.vote.
    - Merkle root for Bank for those who participated in Launch. Vested schedule in Bank. 
    - Reward, allocation of real FVT of 100,000.

**Communications & Marketing**

- Multichain Rollout

  - Work on announcement explanation.
  - DAO infrastructure on new chain. 
  - Flow of each stage. 
  - Assets, Chris C. 
  - Times for Launch and Yield. 

- "Luna Blowup Breakdown by Dr. Nick"

  - Get out announcement.
  - Gated content. FVT Discord.
  - Record session.

**Community**

- MetaSherpa Roles 

  - Schedule meeting this week -- workshop type session.
  - Play to our strengths. 
  - Review payments. 

- Lot of users haven’t gone through verification of their FVT IDs for gated access to FVT Discord.  Ping everyone, even those who aren’t verified.


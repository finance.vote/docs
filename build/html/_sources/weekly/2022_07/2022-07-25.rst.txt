Weekly Updates: July 25th, 2022
*******************************

**DAO Suite**

- **markets.vote**

  - Discussion on BSC IDs and possible offerings to holders as we move Markets to Polygon. 

- Chris S. created new contract with burn function. Team test this week. 
- Call with Dev. Team today after call. 

**Alliances**

- Call with potential partner for post mint utility went well. 
  
  - Tokeneconomics, Nick. 
  - Setup 3 way call for Q&A with potential partner for NFT art series w/irl piece to come.

- Potential partner (fashion)
  
  - This Friday is the ticket mint. 
  - After ticket mint, main mint. Burn your ticket to claim NFT of choosing from series. 
  - Ticketing system with burn system -- new thing we've developed.
  - They will be at The Cryto Roundtable Show Twitter Spaces this Thursday. 
  - Posts & announcements. 

**Operations**

- **TribesDAO**

  - Shibaku
   
    - Coins of Destiny test is live. 
    - Lenny working on improving the UX with Chris S.
    - Improvements to be added when Sebastian is back next week. 
    - Collect new feedback and new screens, Lenny. 
    - Vote on new test.

**Communications & Marketing**

- Catchup call with Athena Labs. After Wednesday session.

  - Session with Ryan & Joshua. New content. 
  - 1 pager and overview videos with new Markets and send to Polygon. 

- Announcements, Markets session with Chris S., Ksenya. 
  
  - More alpha. Prediction Markets. Markets is now finance.vote prediction markets

**SubDAOs**

- **dataDAO**

  - Data for testers, Lizzl. 
  - Fact rewards. Calculate rewards. Launch and Yield and feedback. $5000 of FVT. 

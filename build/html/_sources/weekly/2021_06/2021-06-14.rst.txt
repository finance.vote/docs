Weekly Updates: June 14th, 2021
*******************************

**NFT Auctions**

- Unit tests written. Ready for 2D designs and connection to 3D world. 
- Auction 0

  - July 4th
  - ACTION: land plan by Friday.
  - Start Announcing soon. Ksenya. 

- “NFT Live” Auction

  - July 11th. 
  - Possibly merging event with Unit Global. 
  - ACTION: land plan by Friday. 
  - Start announcing soon. Ksenya.

- Auction 1.0

  - Consideration of a real estate sale. 
  - Search for NFTs.
    
    - Cryptopunk
    - HashMask
    - Olyseum xNFT
    - Carlos Mattos

- Halborn 
  
  - Contracts sent to be audited. Waiting for a response. 
  - 2 contract read-through meetings with the Halborn team completed. Small corrections made.
  - ACTION: Schedule next contract read-through. 
  - ACTION: Message Rob/Steve. 

- Considerations of other peer review options. Ongoing. 

  - “Peer Review Circle” or “Security Cartel”. 
  - ACTION: Setup Telegram group.
  - Possible bug bounty?

- ACTION: Complete “NFT Auctions and the Metaverse” Medium article. 

**Bank.vote & Airdrop**

- Operation: Join the Party

  - How do we deploy the technology? 

- ACTION: Arrange chat about contract with Yuvi, Marek, Adrian about merkle drop & merkle vesting. 
- Contracts written. Next step: tests, interface design, & implementation.

**Influence.vote**

- Merkle identity: contract written, middleware partway done.
- Incinerator: contract written.
- Refactor to make strategies per-proposal instead of per-space.
- ACTION: Gated suggest a token form. Announcement. Incentive? Nick, Joseph. 
- Priority Vote Debrief

  - ACTION: Put out responses this week.

- LDN21 Failed Transactions paid out. 

**Yield.vote**

- Resident Pool
  
  - Debrief after next pulse completion. Speaking to a data scientist. 
  - Liquidity Factory: contracts done, tests done, audit in progress, new interface in progress.
 
    -  *Updating our contract to a liquidity contract allows us to make governance decisions and update contracts without needing to evict everyone. They also allow us to open new pools without needing to deploy new contracts.*

-  ACTION: Need to fund with more FVT. Chris.

**Markets.vote**

- BSC

  - Pokt: still borked, might work with block ranges on filters.
  - GetBlock: trying this this week.
  - Ankr: still borked.

- Stonks on Polygon 

  - What Token Next Votes first. 

    - Suggest a token -- ACTION: Google form for those with IDs and FVT minimum?

  - ChainLink consented.

**Multi-chain**

- BSC

  - ACTION: Make minimal code that demonstrates BSC node failure. Chris.

- Polygon

  - Stonks

- Chainstack Contacted.

**Operations**

- ACTION: Litepaper Final Draft. Nick, Naved. Thursday.
- Dashboard
- Added FVT payment to Halborn to accounting sheet.

**Governance**  

- “The Not So HR Dept.” 

  - ACTION: develop community recruitment process. 
  - ONGOING: Develop Phase 2 People Strategy. (Who do we need to scale again?).

- Update Trello Timeline

  - Convert to “Wenn Chart”.
  - Reflect priority vote in timeline.

- What Token Next

  - Analyst Chamber.
  - ACTION: How to announce it?
  - Engineering team is optimistic that we can do the vote this week with a what-to-vote-on form. 

    - Token gated only

- Potential Designer.

**Community, Content, Marketing**

- DiFi Summit Conference. (https://www.defisummit.com/)
 
  - Tweets published.
  - Announced to TG. 

- ONGOING: GitCoin - Hackathon.
- Twitch Deep Dive AMA June 11th: https://www.twitch.tv/videos/1052816780 
- Discussion on Clubhouse strategy.
  
  - ACTION: Write a new strategy. Joseph.

- Zebu: Joseph working with Zebu on Twitter strategy. 
- Prepare a video for DeFi Summit. Branding/marketing for the video. 

  - Why are DAOs necessary video recorded.

- ACTION: Initiate Discord. Joseph, Ksenya. Thursday. 
- ACTION: Nickbot -- Moving conversations from TG to Discord.
- Bizdev

  - Recruiting

    - Matt
    - Biz Dev spreadsheet sent to Matt. 

- Zebu:

  - Been doing foundation work: messaging matrix, influencer strategy.

Influencers

  - Merkle vesting? 
  - Consortium?

NFT Deck and dApp suite

  - Content done.
  - ACTION: Needs design. Ask Zebu. 


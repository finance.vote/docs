Weekly Updates: September 20th, 2021
************************************

**DAO Suite**

- **markets.vote**

  - Time to start some conversations about the next iteration. 
  - New contracts -- rough drafts. Very early stage not audited. Incentivised test. 
  - Offer the DAO the data and what we think, and ask them how we can improve it. Then, do an incentivised test.  

- **yield.vote**

  - New updates deployed to ENS, change list in deployments (known bug on mobile, in progress).
  - Building new gitlab flow for releases.
  - Version numbering solution required for IPFS deployments.
  - Open-source Python script for access to the data.
  - A new liquidity mining programme - New Basic Pool, Tycoon Pool alongside Resident Pool. Actual APY stat (add new column), Average APY. Campaign and we need Tycoon houses.
  - Need to payout for the test.  

- **auction.vote**

  - Review and refresh the Auction application. 

- **bank.vote**

  - 18th November (Pyramid Phase) First Big Airdrop.
  - At final stages of UX tweaking and contracts now fully audited.
  - Organise team testing event. 
  - Open source vote mining algorithm with community. 
  - Markets one-off payout. Mechanism that rewards early supporters.

- **influence.vote**

  - First draft of binary votes, first draft of forum mechanic, POAP mechanic, NFT displayed on vote. Art not currently displayed in the opinions feed for those with Art IDs. Vote close data display (toggle?). 

- **bridge.vote**

  - Stability update. 

**Alliances**

- Stoner Cats
	
  - Meeting on Thursday with them in their community Discord. DAO Alliance event.
  - Friday workshop. Influence demo.

    - Whitelist NFT IDs or Blacklist. Arrange Influence.vote review pre-cat delivery. 
 
- iTrust

**Operations**

- **Add** Operation: Tycoondoggle
- **Add** Operation: Test Money 
- **Add** Operation: DAOCO
- **Operation:** Unite the Tribes

  - Close to rendering status. 
  - No frame on final, pre-minting state with frame, reveal on mint mechanic. 
  - First draft of minting page.
  - Close to final stages on the tech.
  - Marketing strategy first draft completed. 
  - Interface of the minting screen. Designed and developed.
  - Branding for website. Acts as a wrapper around the tribe-mints.
  - Two Meetings: Tech Meeting 9am UTC (3D team, Senate), Marketing Meeting 9am UTC (Wednesday - invited).
  - Source domains. 

**Communications & Marketing**

- Newsletter Update. 
- The Ungoogling.
- Tycoon Pool Launch. 
- Harberger Taxes and the Xenia Programme. 
- Stoner Cats PR - ATL - Send PR draft to Mack and Lisa. 
- DAOs and NFTs - PR piece.

**Community**

- The Programme.
- Kick-off Projects.
- Community Fund.
- Orchestrated conversations.
- Radio Review. 
- Community Calls.

**Governane**

- Governance Framework Update.
- Community Payout Structure - Use Ropsten / Gourli IOUs.

**Priorities**

- Chris S. - To make sure tech is useable for website. Test contracts. Influence Polishing.
- Naomi - Product Unite the Tribes and Influence Polishing.
- Ksenya - Community Artist Project.
- Lenny - Brand Guidelines for website.
- Marta - Testing Influence. Guides for influence. Competitors for Yield.
- Joseph - Twitter schedule for the week and delivery.
- Naved - User study of Yield, review of the materials. Editing on documentation.
- Nick - Bring the community to life. 


Weekly Updates: September 6th, 2021
***********************************

**Bank.vote & Airdrop**

- Bank QA.
- Internal team testing. 

**Influence.vote**

- Adding a discussion element to Influence to facilitate early proposal formation.
- Making NFT visible when voting.
- 3D Team

  - Designs are done. 
  - Arrange product meeting on strategy for NFT series project. Tuesday 9 UTC.  

    - Discussion on economics of NFT minting. 
    - Content for a landing page. 
    - Marketing and messaging strategies.

      - Fair NFT launch.
 
    - Twitter account. 
    - High gas prices? 

- Issue with BSC IDs voting for Influence vote. 
- Influence 2.0 UX. 

**Yield.vote**

- Yield farming article getting good traction. 500 views on first day. https://financedotvote.medium.com/finance-vote-announces-xenia-a-90-million-fvt-liquidity-program-c1d9e2c87167
- Noon UTC. Deploy Yield contract. 

  - Notify Alex K.
  - Backup IPFS links. 
  - Add quick guide to website. 

- Deploying liquidity factory contracts. 
- Announcements

  - Announce block numbers tomorrow. 
  - Medium article. 
  - How to make more exciting. 
  - TG -- get out ASAP. Get activity up.
  - Discord.
  - Countdown. 

- Retake Ethtrader banner Tuesday. 
- iTrust

  - 50mil cap on their Resident Pool. 

- More easy step-by-step documents/videos/tutorial images/slideshow etc. Something that could be forwarded on TG.  

**Multi-chain**

- Bridge is up. Has not gone down for a week. 

**Community, Content, Marketing**

- More educational video assets. 



===================
Yield farming
===================
**Important definitions:**
  * **APY** - Annual Percentage Yield, amount of yield you would get on your principal annualised
  * **APR** - Annual Percentage Rate, it's similar to APY but it doesn’t consider compound interest
  * **LP** - Liquidity Providers, users locking their funds into Liquidity Pool 
  * **Liquidity Pool** - a smart contract containing funds, powering a marketplace where users can exchange, borrow or lend tokens. `Click here for more information <https://financevote.readthedocs.io/en/latest/content/terminology/liquidity.html>`_ 
  * **Liquidity mining** - process of distributing LP tokens to Liquidity Providers
  * **Collateral** - a borrower's pledge of specific property to a lender, to secure repayment of a loan

In a nutshell, yield farming is a reward scheme. Farmers(users) can lock up their funds in liquidity pools and gain fixed or variable interest in exchange. 

.. admonition:: Analogy

   Imagine you are a real farmer. You sow grains in your field and wait for them to grow, then you reap the crop or yield. 
   
   Now compare it to our situation - the farmer is the user, grains are tokens, the field is a liquidity pool, and yield is profit.
   
   Users put tokens into a liquidity pool, wait for the interest to grow, and then gain profit.

| We can define yield farming as lending cryptocurrency via the Ethereum network. It is like being a bank, with the difference being that the lender doesn't meet the borrower and the specific borrower doesn't return funds to the specific lender. 
| Yield farming is based on liquidity pools which are smart contracts holding funds. 
| `Click here <https://financevote.readthedocs.io/en/latest/content/terminology/liquidity.html>`_ for more information on liquidity pools.

.. important:: Investing in cryptocurrency is not yield farming.

Why do we even need yield farming?
-----------------------------------
| Of course it would be much easier to just put your crypto in a wallet and see what happens. You could make money when your tokens go up in value and wouldn't necessarily need yeild farming to make profit. This is also a much easier route for those who are new to cryptocurrencies. 
|
| Stepping into yield farming is highly risky but also highly profitable. You've probably heard this a thousand times, but high risk - high reward  **Thus, yield farming is recommended for more experienced users, who know what is what.** But once you feel that you are ready to step in, you should be aware of some basics to yield farming.
|
| The main idea behind yield farming is to make a profit, of course. Simply lending funds to the market won't bring a huge profit, unless the pool is very deep and a user's contribution is very large since commission from lending funds are often below 1%. 
|
| Now imagine that you have some extra money and you would like to deposit it into a savings account. It is quite obvious that you would like to earn as much interest as possible, so you look for an account with the highest APY.  APY along with APR are indexes used in cryptoworld as well. They represent rates of returns and help you decide whether a strategy is profitable or not. 
|
| With traditional savings accounts, APY fluctuates. They are typically somewhere around 0.1%, or if it is a very lucrative offer, then it could be somewhere around 2-3% at best.
| And this is where a big difference with yield farming emerges. Compared to savings accounts, yield farming has the potential for insanely high returns like 100% APY.  Though, such returns are associated with higher risk and require some tactics to grasp. 
|
| Let's discuss then, how can users make significant profit from yield farming?
| The key word is: **strategies**.

Strategies
----------
| By strategies, we mean moving funds between different DeFi protocols. It is either moving tokens earned from providing liquidity to other pools or by burning some earned tokens to receive back your locked funds and move them. This is really up to the user, since it requires a lot of market analyse, good intuition, and a hint of favourable wind.
| The most profitable strategies usually require moving between at lest a few DeFi protocols (e.g. yield.vote, Uniswap, Compound, Curve, etc.).

.. admonition:: Analogy
    
    Moving funds between different protocols or swapping coins could be likened to a crop rotation. That's a farmer's practice of growing a series of different types of crops in the same area on rotation, which maximises yield as a result.


| You probably know by now that there are many different DeFi protocols, and they offer different actions to users like lending and borrowing, supplying to liquidity pools, staking, and more. As we mentioned before, strategies are based on moving between different DeFi protocols. To earn a profit, a user can lend funds to a lending protocol, borrow some funds and trade them right away, or stake LP tokens. A user could combine these strategies as well to maximise profits.
|
 **Possible strategies::**

    * Supplying to liquidity pools: You can provide liquidity to a liquidity pool and gain commissions from token swaps that happen in that pool. 
    * Staking LP tokens: Some protocols allow you to stake LP tokens and reward you in their own LP tokens, which could be further stake in yet another protocol. To put this in other words, you could stake funds in pool A and get Atokens in return, then stake Atokens in pool B and get Btokens in return and so on. This way, you get commissions from different pools at the same time. 
    * Lending and borrowing: You can supply funds to a lending protocol for the purpose of earning a percentage on their capital or borrow from a lending protocol to leverage (i.e. borrowing funds to invest).


.. caution:: Please remember that yield farming strategies could potentially become unprofitable in a matter of days or even hours. Keep an eye on your locked funds and react quickly.

Liquidity mining
----------------
| We will only briefly cover this topic. People often say that yield farming and liquidity mining are the same thing. Actually, they differ, but it's safe to say that both liquidity mining and yield farming have the same purpose - to maximise returns by earning tokens. Also, both operate within the DeFi sector.
|
| Liquidity mining is the process of gaining new LP tokens operating on the Proof-of-Work algorithm, while yield farming uses different DeFi apps, including liquidity mining ones. 

Leverage
-------------
| Leverage is a common yield farming strategy used to increase profit. Users borrow external liquidity and add it to their liquidity for the purpose of increasing  potential returns. This way it helps increase a user's share in a pool and LP token amounts as well.
|
| Let's see this over an example:

.. |ProvideLiquidity| replace:: provide Liquidity
.. _ProvideLiquidity: https://financevote.readthedocs.io/en/latest/content/terminology/liquidity.html#setting-up-a-pool

.. admonition:: Example

    When Carl provided liquidity into the pool (for more information, see the example: |ProvideLiquidity|_) he added 2000 USD, but he wasn't satisfied with his percentage share in the pool. So, he borrowed 1 ETH for 1000 USD and added it into the pool, now making it 2 ETH and 2000 FVT.

    Thanks to this move he now has a larger share in the pool than before and thus gains higher commissions. 
  
Collateral
^^^^^^^^^^
| Whenever you’re borrowing assets, you’ll have to provide **collateral**, which covers your loan and acts as insurance for it.  In the case where a borrower fails to pay back a loan, collateralization serves as a compensation to the lender where the lender then keeps the collateral.
|
| Depending on the lending protocol, there are different rules for collateral rates. For example, some lenders require little to no collateral, while others can require up to 400% of the lending value. 
|
| It's worth mentioning though that over-collateralization helps diminish the risk of severe market crashes and also saves the borrower from liquidation of its collateral on the open market.

Liquidation
^^^^^^^^^^^
| **Liquidtaion** happens when the collateralization ratio (collateral:lended value) drops below a certain threshold, which means when a user's collateral is insufficient to cover the amount of their loan. 
|
| There are some methods to avoid liquidtaion:

  * over-collateralize to prevent the collateralization ratio from dropping below the threshold amount
  * use less volatile assets like stablecoins (the more volatile the asset is, the higher are the chances of liquidation)

Risks of yield farming
-----------------------
| Again, yield farming can be very viable, however, the most lucrative strategies come with extensive strategy planning. Let's introduce some risks of yield farming:
1. **Liquidation**. As mentioned above, liquidation causes losing collateral.
2. **Asset losses caused by smart contract bugs**. Yield farming relies on smart contracts which, like any software, is exposed to potential attacks and suffer from random errors. Thankfully, doesn't occur frequently but users should be aware of these kinds of potential risks.
3. **Value of tokens changing**. The desirable outcome is for token value to increase, but sometimes it is the other way around which leads to fund loss.
4. **Block malfunction**. Whenever a block experiences a malfunction, the entire ecosystem suffers. Within the ecosystem, every block is connected with one another. This idea helps make it permissionless, but it demands collective responsibility.


Is Yield Farming Safe?
-----------------------
| Yes and no. 
|
| If you come into yield farming without understanding its mechanisms, it will definitely backfire on you. Learning any form of investment takes time, effort, and substantial research to understand concepts and how markets operate.
|
| Yield farming is definitely a dynamically changing market, with a lot to offer and a lot to loose. Understanding the terms of smart contracts on your own, rather than relying on third-parties, is a good idea.
|
| Knowing these basics should help you to make better decisions with your funds within the cryptoworld.

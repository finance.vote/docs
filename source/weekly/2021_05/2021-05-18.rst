Weekly Updates: May 18th, 2021
******************************
**Yield.vote**

- Incentivized Test

  - Test closed
  - Found bugs resolved 
  - Preparing to pay out incentivized rewards

- Citizens Priorities Vote

  - Vote completed and results compiled
  - Results posted to Telegram and Twitter

- Resident Pool

  - Launch details posted to social media
  - Yield parameters set to metrics voted upon in Citizens Priorities Vote.  Launch Parameters (14 day pulses, 1m FVT per pulse, Sushi Pool 0 LP entry, 1000 SLP cap)
  - Launched and now live. Approximately $650,000 currently staked at time of writing 
  - Resident Pool Medium article published: https://financedotvote.medium.com/finance-vote-resident-pool-launch-sequence-5ea94a8a5236 
  - Explainer video posted to Youtube: https://www.youtube.com/watch?v=FrGzmjnq2Ac&t=155s 
  - “Image Not Displaying” issue resolved 
  - New release update with the following features / upgrades: 
    
    - Sortable tables
    - Fixed failed txs error
    - Various statistics bugs fixed
    - Minor style improvements
    - Performance increase (reduced calls to infura)

- Discussion on further social media coverage and posts

**Markets.vote**

- BSC Ballot #7 winner announced 
- ETH Ballot #26 winner announced
- Discussion related to the possibility of a launch to Polygon network 
- bridge.vote added to priority list 
- BSC infrastructure discussion. Create “The State of Binance Smart Chain” post to update BSC citizens on specific progress…. Potential Influence.vote to decide on trying to fix BSC or move to another chain…  
- Preparing to announce Bounty on GitCoin to scale multi chain deployments
- Discussion on Kusama deployment viability 
- New release update with the following features/upgrades: 
  
  - Share vote to twitter button
  - Many bug fixes
  - Default art now showing
  - Performance improvements (caching identity info)

**Auction.vote** 

- Discussed novel cost-efficient solution to batch minting using immutable.com or other scalability solution
- Building of NFT Auction interface
- Olyseum token auction discussion 

**Influence.vote**

- Episode 2 ethereum citizens vote for designing yield parameters for Resident Pool completed
- One week code clean up and refactoring begins 
- Prepare for rapid deployment of influence votes 
- Discussion related to “normie friendly” v2 influence development trajectory 

**Community, Content, Marketing**

- Zebu engaged as PR partners
- Community Call completed and recording on SoundCloud: https://soundcloud.com/theogoodman/fvt-community-call-may-14-2021-proof-of-selfie 
- Branding meeting scheduled for next week 
- Soon purchasing banner on r/ethtrader Reddit to promote finance.vote. This Reddit uses Harbinger Taxes as we do in Resident Pool 
- Purchased requisite donut tokens to be able to purchase r/ethtrader banner 
- Finalizing design of banner to be placed on r/ethtrader 
- Discussing possibility of proposing r/ethtrader collaboration 
- Meeting with potential UX design consultant 
- Create Video for DeFi Summit and begin work on Keynote
- Preparing quiz competition with the crypto personality test and synchronising with airdrop

  - Preparing designs 
  - Coordinating with Zebu

- Arrange a “Deep Dive AMA” to be held in our home Telegram group
- First “Crypto Philosophy Corner” in Discord 

**LDN21**

- Results finalized to PDF. Preparing to publish
- Post our press release on Medium
- Continue discussions with mainstream press 

**Partnerships**

- Preparing Olyseym partnership announcement  
- Build FVT partner deck for the dApp suite

**Community Fund**

- Move to a new selection and payout structure
- Selecting this week’s top submissions
- Promote engagement and recruit more content creators

**Governance**

- Continuing work on Timeline to soon be made public 
- Continuing work on Governance Map to soon be made public
- Continuing work with lawyers to establish our legal entities 

**Hires**

- @HelloImChris and @SaviorCrypto brought into the core team as creative content producer and token economics analyst
- 2 new developers to be added to the front end team
- Recruitment search for: rust developers, product designers, solidity developers, and open call for crypto and data analysts 



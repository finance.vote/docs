Weekly Updates: April 19th, 2022
********************************

**DAO Suite**

- **bank.vote**

  - Transitioning payroll payments to Bank.
  - Tech updates. 
  - Payments for MetaSherpas. 

**Alliances**

- Potential NFT project. 

  - Discussion on them using Influence. 
  - Potential for using other dApps down the road. 

- Update on talks with potential membership DAO. 

  - Minting NFTs.
  - Schedule of releases.
  - Exclusivity.

- Updates on talks with other potentials partners & investors. 

**Communications & Marketing**

- Doing a Twitter Spaces and demo for potential Influence users.
- Writing about our own experiences.
- Videos for next week.
- Turning Weekly Updates into short highlights. 
  
  - Maybe video or PDF learnings. 
  - All that we have achieved and all that we have learned. 
  - Talking about what we do. 
  - DAO life. Meta. 

- Educational content.
- Website
 
  - Meeting today 4pm. Nick, Naomi.
  - Audience segmentation and offering for each. 
  - Landing page.

- Seasons
  
  - Announcements for Seasons.
  - Season 1: Release our FactoryDAO NFTs.
  - Between Seasons is offseason. 

- Season 0 -- In Review

  - Make next week a gated reflection week. Only gated for the week.
  - Reflecting on our 18 month run. Where are we at, where are we heading, values, the future, the bull run as a whole.  
  - Exclusive FVT party. "After After Party" kind of vibe. 
    
    - DAO DJ set, games, competitions and party.

  - Tues. ~ Thurs.
  - 1/2 day for us (internal review) and 1/2 day with public.
  - Charlie and Conrad.
  - Structure time in for writing.
  - Plan out today in PropaganDAO meeting. Sketch out schedule of events, workshops. 
  - People sharing about DAO experiences. 
  - Novel practices in DAOs. 

**Community**

- Community angle of training DAO leaders and DAO tooling. 
- Give a last week for obtaining a Season 0 pass. 
- DAO facing calls and planning and discussing with the community.

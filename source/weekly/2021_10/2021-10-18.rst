Weekly Updates: October 18th, 2021
**********************************

**DAO Suite**

- **markets.vote**

  - Continued work on markets. 
  - Working with community to enhance.
  - Market data. 
  - The Programme.

- **yield.vote**

  - Proposal engagement. 

- **auction.vote**

  -  Zebu suggesting a potential partner to use auction.vote for single item auction for NFTS. 

- **bank.vote**

  - Airdrop for Nov.18th.
  - Community test of Bank soon. 

    - Get addresses. 

  - ACTION: Team Test this week  

**Alliances**

- Stoner Cats

  - Vote.
  - POAP system
  
    - **ACTION:** arrange product meeting next week.  

**Operations**

- New Operation

  - Stress-tests.
  - Pre-compute merkle trees. 
  - Rarity. 
  - **ACTION:** Contract / full stack read through. Chris S., Nick, Naomi. Tues.  

- TribesDAO
  
  - Interactive elements over to the devs.
  - Content / lore.

- Content

  - Influencer Deck. 
  - Early Population Discord Strategy.
  - Teaser Launch - Roll Out website.  

**Communications & Marketing**

- **ACTION:** Create image for Weekly Update.

**Community**

- Zak to discuss NFT series next week. 

**Priorities**

- **Nick** - Create DAO Suite and What is finance.vote posts. Generate proposal for markets 2.0. Prepare and deliver The Programme sessions for this week. Prepare for NFT Programme week. Support in TribesDAO content creation.
- **Chris S.** - Rendering, complete minting page, and test NFT system even more for new operation. Empower data journalism.
- **Naomi** - New Operation/Tribes DAO landing page, Zebu New Operation and Twitter push, and New Operation marketing materials.
- **Chris C.** - Begin to populate TribesDAO Discord. Curate New Operation Twitter content. Work out reward/presale structure for OG TribesDAO Discord members. Welcome message/navigation image for FVT Discord.
- **Naved** - Payroll, organization, Weekly Update & priorities, Imagination Time 2x, and Discord & community engagement.
- **Marta** - Further work on NFT Projects research and anti-scam tips.
- **Lenny** -  Do research into POAP and how we can make our attendance NFTs better. Support the New Operation design.
- **Joseph** - Schedule tweets for the week, find best recording solution for events, create plan for content distribution of The Programme, & plan interview with Zak.
- **Matt** - Finalise integration of ZenDesk and watch all the onboarding video series. Continue to develop future of work narrative. Speak to job board applications. 





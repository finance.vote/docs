Weekly Updates: August 2nd, 2021
********************************

**NFT Auction**

- 3D Team starting this week. "Team Virtue".
- Book meeting for Tuesday.
- Reviewing potential OKRs for team.
- Branding meeting today Aug 2nd. 
- Prepare for Demo.
- Design directions. Interface designs.

  - Land designs by Friday. 

- Agenda: Mon research, Tues branding, Wed start design.
- Document for design suggestions for Wednesday. 
- Make deck, website, marketing content. 
- 2D

  - Space for those not using 3D space. 
  - Background --  we could use a color or overlay the 3D world onto the 2D world. Static background. 

**Bank.vote & Airdrop**

- We have designs for bank. (add commit popup and NO vesting journey)
- Airdrop

  - Complete vote mining design Aug. 6th.
  - Types of vesting: Automatic Vesting, Manual Vesting, No Vesting. How to use. 
  - OKRs for Airdrop.​​​​​​​
  - Collect addresses and assign token amount. Aug. 6th.
  - Airdop UX design ready to be reviewed. 
  - Using basic interface and existing contract for this launch. 
  - Aiming for Friday 13th for Airdrop.

    - Includes anyone who has voted in any Influence or Markets vote ethereum or BSC chains. 
    - Users will come to Bank to claim. 
    - Marketing 

      - Can announce Airdrop this week. Will help us to push the Influence vote for what token next. 
      - Initial teaser followed by what token next vote. 
      - Zebu. Will build marketing around OKRs. 

**Influence.vote**

- Vote mining for Influence votes. 
- What token next? 

  - Wed -- UI changes and BSC voting for What token next? 
  - Start announcing during the week and aim to launch vote on Friday. 
  - Allow 5 days for the vote. 

- Where is the product at?

  - Multi-chain option has created delays. Looking at some solutions.

- Work with partner.

  - Announcement draft completed. Partner to approve. 
  - Will announce to public soon. 

**Yield.vote**

- Ready to deploy Wednesday. Start new pool. New interface. 

  - Aim to push one week after. Aug. 13th
  - Comms Mon Aug. 9th. Prepare to announce. 

- Itrust

  - All their Basic Pools work as is.
  - Meeting Tues. Aug 3rd. 

- Turn new pulse into recurring announcement.
- Update accounts screen. Front-end. 
- Aim to be feature complete by Friday Aug. 2nd.

**Markets.vote**

- ACTION: Schedule a Markets design meeting for next week. Open to public on Discord. Aim for Tuesday Aug. 10th. 

  - ACTION: Announce on TG and Discord. 

**Operations**

- New Meeting Format

  - Smaller shorter focused meetings.
  - Breaks after 30 minutes
  - Clear on departments and departmental meetings.
  - Begin imagination/hangout time. Shedule for Friday. Naved. 

- Moving over to Proton mail from Google. 

  - Research single sign-in. In discussion with Proton support. Naomi. 

**Governance**

- OKRs - onboarding plan on Mon.
- Discussion on more public meetings.

**Community, Content, Marketing**

- Schedule call for Thursday to discuss. Matt, Ksenya, Joeseph, Chris C, Naved, Naomi. 
- Possible. Weekly theme for the week.

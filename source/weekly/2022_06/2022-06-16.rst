Weekly Updates: June 16th, 2022
*******************************

**Alliances**

- Biz Dev updates and discussions. 
  
  - Potential. Would do a Protocol DAO for them. An NFT MetaDAO. 
    
    - NFT DAOs and subdaos funding.
    - Protocol governance using the token, launch token mass distribution.
    - Giving them more demos. 
    - Discuss commission with them. 

  - Potential

    - NFT art series by an artist. 
    - Chance to burn the NFT after holding for some time to claim a real art piece at a party. 
    - Mint. DAO Infrastructure with Influence.

**Communications & Marketing**

- finance.vote prediction markets 
  
  - Narrative. SBT.
  - Get content together. Everything we have about markets.vote into document (Medium, Youtube, Substack, Analysis).
  - Get interest back into this. Get community excited about relaunch of Markets to Polygon. 
  - Vote proposals for Markets on Polygon. Finish and allow community to review. Votes to go live on Wed. 

- Season 1 Planning

  - Build with community. 
  - Discuss when and how to begin planning. 
  - Start collaborative document to drop in materials and ideas. 

- Discussions on market conditions, positions, and narratives. 
- DAOs are gonna lead the next cycle and we are building the tech.
- Help us build and get involved.

**Community**

- Friday Twitter Spaces on launching apps on Poly, markets.vote?
- Announcements for Sketchy Apes / Factory DAO workshop.
- DAO Gossiper. Announce. DAO leaders try to add a submission. 
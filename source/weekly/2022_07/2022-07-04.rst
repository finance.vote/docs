Weekly Updates: July 4th, 2022
******************************

**DAO Suite**

- **mint.vote**

  - Discussion on test net with potential partner. Pushing back. Working on some issues with Mint.
  - Whitelist for Season Pass ready. Close whitelist Season 1 on Wednesday. 
  - A popup disclaimer. DYOR, terms of service type thing. Begin collab doc to brainstorm ideas. 

- **markets.vote**

  - Testnet for Markets and SBTs.

- **bank.vote**

  - Issues with Bank. 

    - Version 2 conflicts. Adjust Bank to various contracts. Adjust subscriber. Compatibility issues.

- MM on Poly and Avax issue. Sovryn, FVT, FactoryDAO, Mint to .org from .xyz like mint.factorydao.org. Push to new versions.


**Alliances**

- Discussion on upcoming mint dates for various upcoming alliances. 
  
  - One happening on ETH network. Getting art and whitelist, designs/frontend.
  - Add more addresses for whitelist and for Gold list. 
  
- Potential partner, a private membership DAO, wants to mint in about a month for IRL event.
 
  - Art, marketing.
  - Help with the art and the Mint. Minting page and tweaks to website.

- Potential partner wanting to use Bank on the 25th on Avalanche. 
- Potential asking for a Bank Airdrop. Recommended by Zebu. Not possible this week. 

**Communications & Marketing**

- Push our minting partners to market us, announcements. 

**Community**

- Updates on MetaSherpas. Call Tues. 11am UTC. 
- Collaborative doc. "2022 FactoryDAO Season Zero Governance Game". Everyone write in vote proposals. 

  - Votes open by Thursday? Open for 7 days
  - Hour slot on Friday for Dr. Nick to Speak about. 
  - Experimenting with our gov tools. 

- Season 1 Planning. Discuss open planning in Discord on Friday public call. Announce Season 1 "FactoryDAO Gov Review".
- Right after: Open call Shibaku Discord – Friday – relaxed community call. 
- Content calendar & Discord content & routine times we coordinate in the Discord. 
- Turning up in Discord and speaking more there.



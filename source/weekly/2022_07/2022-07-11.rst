Weekly Updates: July 11th, 2022
*******************************

**DAO Suite**

- **markets.vote**

  - Discussion on Markets.  

- **influence.vote**

  - Discussion on Influence Realms. 

- **bank.vote**

  - Have 2 DAO leaders withdraw their FVT payments from last month's payment.

**Alliances**

- Updates on upcoming Mint partners. 

  - Myna Mint happened on Thurs. 
  - Discussing and waiting to hear back on other 2. 
  - Should be Aug. 1st for the private membership DAO. 100 pieces of art. 

- Mid Aug. for potential Bank partner. 

**Communications & Marketing**

- Athena Labs

  - Have been writing Tweets.
  - Discuss with them about a content/announcement scheduler. 
  - Create master doc for Tweets. 

- Announce Myna Mint. Anyone can Mint on Avalanche, follow Nick's article. 
- DAO Gossiper open for two weeks. DAO Leaders try to submit entry. 

**Community**

- Get out Governance Game votes. 

  - Start discussion. Setup community chat. 
  - Run for 2 weeks. 
  - Begin writing Tweets and announcements. 




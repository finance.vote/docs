Weekly Updates: May 23rd, 2022
******************************

**DAO Suite**

- **Multichain**

  - Avalanche Incentivized Testing: Launch, Yield, Bank

    - Launch ready to go. Some issues being worked on for Bank. 
    - 6th of June Spark is going live with vesting. 
    - Running some more tests this week. 
    - 24th - Launch. 
    - Aim 30th - Yield. 
    - Change Auction logo to Launch logo. 
    - Redeploying new contract tomorrow. 
    - Block time? What time we want to launch? 
    - Testnet Avalanche and faucet. 
    - Team try out onboarding today with MetaSherpas.

- Product Roundup meeting. Wed 9am UTC. 
- Product meeting today. Nick, Naomi, Chris S., Matha, Lenny.

**DAO Alliances**

- From Biz Dev Roundup meeting, create one sharable doc of current and potential partnerships/alliances for other potentials and relevant parties and another version for public and community. 

**Communications & Marketing**

- Avalanche Incentivized Testing
  
  - Nick Tweet storm.
  - Document walk through of 3 dApps. 
  - Priority, announcement and step-by-step idiot's guide. 
    
    - Done today.
    - Lenny to send screenshots for step-by-step guide. 
    - How to participate. Money to be made. 
    - Nick writes intro.

  - Test blog post. 
  - Schedule Twitter Spaces for 2pm UTC. To dovetail with Launch. Avax and DAO infrastructure.
  - Discord/TG Announcement for FV and FactoryDAO, Ksenya.
    
    - Signal intent in general and then we put you in testers channel.

  - Get as many people as we can. Recruit. 
  - 5k FVT shared prize pool, 2500k top prize shared between top 10 testers and the remaining 2500 split between the rest. 
  - $1k additional $FVT will be distributed to addresses to FVT NFT ID holders.
  - Season 0 Pass holders can help coordinate.
  - Website. Chris C., Conrad. 
  - 1 Testers channel, setup Testers role.
  - Spoke to Avalanche UK about backing us.
  - Friday, 9am UTC, Charlie and Athena Labs & Chris C. and Nick.
  - Biz Dev. Roundup meeting, Tues. morning. Naomi, Nick, Matt, Naved.

**Community**

- Idea for basic tasks available to non ID holders so they can earn FVT and work their way up to an ID.  “Start Here” board on Dework and ask MetaSherpas to fill it in with simple tasks & fund them from community fund.
- Next week some Programme events. More Twitter Spaces? Someone to come speak?
- The DAO Gossiper, everyone can submit to Weekly Update, Ksenya.
- Fun influence vote on top NFT projects on Avalanche chain? 

**Governance**

- Monday 2pm. Demoing of Alegra. 



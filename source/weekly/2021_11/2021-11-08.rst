Weekly Updates: November 8th, 2021
**********************************

**DAO Suite**

- **bank.vote**

  - Lenny update.
  - Discussion on vesting. 
  - Run simulations. How much will it cost to initialize? 

- **bridge.vote**

  - Bug fixes.

**Alliances**

- **Stoner Cats**

  - Still doing their vote.
  - UX requests

    - Naomi creating log and prioritizing. Numerous fixes will come with Influence 2.0. Share link to Ksenya and their devs.
 
 - Asking for votes for multiple contracts. Combined votes. Ronin and Stoner Cat holders.

   - Back-end available for this. 2 contracts on the same chain. 
   - Continue to develop the tech. 

- **TotemFi**

  - Create nation.
  - 2 proposal creators. 

- **New Partner**

  - Ran successful bank test with them.
  - Lenny working on onboarding.
  - Matt to be point of contact, Naomi to bring him up to speed.
  - Chris S. to make example spreadsheet.
  - First run this Friday (target).  
  - Filetypes. 
  - Get csv data from them. 
   
- **Potential Partner**

  - Want to launch their own NFT series for their DAO, voting.
  - Maybe they are a tribe.
  - Maybe they are first mint.vote partner.
  - Very different style
  - 50/50 split sale.
  - Secondary market goes to public goods fund.
  - Naomi to send proposal today and Naomi potential meeting.
  - Potential dates.

**Operations**

- TribesDAO

  - Website

    - Finished and now live: https://tribesdao.com/#/shibaku.  
    - Reveals.  

  - Rendering.

    - Post-processing.    

**Communications & Marketing**

- Updates from Chris C. on influencers, Youtubers, Twitter, Discord, etc., and strategies. 
- Discussion on prize, competition, and award ideas.
- Discuss Twitter Spaces next week. 
- Zebu to take over Twitter in 1-2 weeks. Matt to continue to manage for now. 


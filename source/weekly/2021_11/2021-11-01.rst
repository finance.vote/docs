Weekly Updates: November 1st, 2021
**********************************

**Operations**

- Unite the Tribes

  - Test mint ready by Tuesday/Wednesday.
  - 50 x Shibaku 1 as a test on Rinkby, all FVT holders can mint.
  - Website still to be deployed for TribesDAO and Shibaku.
  - Final test render of Shibaku. Attribute - Value Pair. 20k. Conduct an audit of the NFTs.
  - Content for TribesDAO website needed. 
  - Eligibility Checking Component - Indicator of interest.
  - Roadmap near completed.
  - Redacted timeline. 
  - NFT Alliance.
  - Influencer Deck and strategy.
  - Possible mint dates. 

**Communications & Marketing**

- Unite the Tribes

  - Possible promotional competitions with awards. 
  - Discord collaborations. 
  - Reveals. Possible: weekly reveal schedule. 

    - Tokeneconomics reveal. 

  - Alex Kruger.  

- Focus on Discords and get community activated. 
- Zebu. 
- The Programme 
    
  - Governance focused week. 
  - Focus Day: Vitalik's post on DAO Cities.
  - The Programme image. Matt. 
  - Imagination Time - Wednesday. Also Saturdays? 
  - Talk about bank.vote and airdrop. Thursday.
  - Talk about Governance Framework. Thursday.
  - Times. 
  - Dev updates from engineering team. 
  - Next week's theme: DeFi Live.

    - Discuss Tuesday.
    - Content and merchandise.

- Cleaning up Discord channel and making it more meritorious to fit governance goals. 





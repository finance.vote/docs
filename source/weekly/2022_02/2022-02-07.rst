Weekly Updates: February 7th, 2022
**********************************

**DAO Suite**

- **markets.vote**

  - Updates.
  - Errors.
  - Announcement about decommissioning markets 1.0.
  - Tokens to be claimed. Claim and not vote.  

- **yield.vote**

  - Updates.
  - New vote?
  - Final yield pulse will pause until new voting strategy. 
  - Basic Pool

    - Basic Pool specs vote.
    - Stake weighted votes.
    - Meeting Dev team. Tuesday.

  - Discussion on governance minimized systems.  

**Communications & Marketing**

- Pitch deck to be ready today. 
- White paper. Work continuing this week. 
- SubDAO focus.

**Community**

- Restructure The Programme (the Programme Chat). 
- The Programme calls for this week. 
- Session with Stoner Cats. 
- Claiming rewards on yield, markets. 

**Governance**

- Skiff, Dework, Notion.
- Discussion on possibility of new token-gated server.
  
  - New space to work/discuss on new projects still in stealth mode. 
  - Minimal channels. 
  - Gate General & Announcements channel at 10K FVT. Higher amount for other channels such as Co-design space. 



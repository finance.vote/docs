Weekly Updates: February 14th, 2022
***********************************

Happy Valentine's Day :)

**Alliances**

- In talks with potential partner. 

**Communications & Marketing**

- Plan out 6 week showcase.

  - Content
  - Meeting today. 2pm. Ksenya, Matt, Naomi, Nick. 

- Building exposure.
- New social media person. Will help with social presence. 
- Twitter ads. Targeting the right audience.
- How to DAO. Onboarding. How to build a DAO and why you should use our tools. 
- Step-by-step instructions on how to launch a DAO with us. Simple.
- Dev team meeting at 1pm. What to work on in the next 2 weeks. 
- DAOs are a place of opportunity. Opportunities will bring people in. 

**Community**

- ​​​​​​​The Portal presentation. Thursday. Goes to community for vote.
- This week's The Programme. 
- Governance AirDrop. 

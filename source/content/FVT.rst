Finance.Vote Token
====================
.. |A| image:: ../images/launch_icon.png
    :height: 15px
    :target: https://financevote.readthedocs.io/projects/launch/en/latest/

.. |B| image:: ../images/bank_icon_dark.png
    :height: 15px
    :target: https://financevote.readthedocs.io/projects/bank/en/latest/

.. |I| image:: ../images/influence_icon.png
    :height: 15px
    :target: https://financevote.readthedocs.io/projects/influence/en/latest/

.. |M| image:: ../images/markets_icon.png
    :height: 15px
    :target: https://financevote.readthedocs.io/projects/markets/en/latest/

.. |Y| image:: ../images/yield_icon.png
    :height: 15px
    :target: https://financevote.readthedocs.io/projects/yield/en/latest/

Finance.Vote Token is the ERC20 utility token used in the dApp Suite. A name of it voted for by the DAO prior to issuance (symbol: FVT).

The three design pillars of the FVT ecosystem are:

* **Prediction and Market Discovery** - DAOs handle, issue, and utilise digital assets. A core component of doing this effectively is to build good knowledge of the price of assets. The FVT ecosystem facilitates social consensus around the expected future value of established and upcoming digital assets.
* **Second Layer Governance** - finance.vote offers a suite of governance tools that can provide a voice to all token holders on any network. The second layer governance tools create a route from rough consensus and dialogue to high-stakes on-chain governance decisions.
* **Decentralised Social Trading** - The finance.vote social trading system will provide groups of any size with the ability to share market information, pool assets, and make collectivised trading decisions.

DAO members use FVT token in each of the applications, specified below:

* |A| **launch.vote** - For any digital assets sold on launch.vote, a portion of the purchase amount is used to purchase FVT off the market. This FVT is then burned. (e.g. 1,000 ETH is spent in the auction to purchase TOKEN-A and 5% of the ETH spent (50 ETH) is converted to FVT and removed from supply).
* |B| **bank.vote**- Projects wishing to use the vesting schedule system may be required to pay or burn FVT in order to do so. The majority of FVT emitted during the token generation event were subjected to a vesting schedule. Additionally, future investors, employees, and contractors may receive tokens on vesting schedules.
* |I| **influence.vote** - Token holders can use FVT or an identity from markets.vote to “influence” proposals concerning the DAO through token weighted voting or other reputation mechanics.
* |M| **markets.vote** - Access to the markets.vote dApp is gated by a decentralised identity token (an NFT) purchased with FVT (that is subsequently burned) from an auction system. Holding a decentralized identity token entitles users to a vote in a weekly set of prediction markets. Identity holders that make correct predictions share a reward pool of FVT.
* |Y| **yield.vote** - Users can use their FVT to earn token rewards in various liquidity pools. Projects wishing to create their own pool must burn a certain amount of FVT to create a partner pool.

Additional Revenue Streams
-------------------------------
finance.vote has and is considering additional revenue models for the dApp suite as the DAO evolves and interacts with market forces. Assets go to the finance.vote treasury to be used by DAO members. Some of these are listed below.

* |A| **launch.vote** - A percent of the liquidity generated by sale of any fungible tokens may be collected as liquidity provider (LP) tokens and stored in the finance.vote treasury (e.g. if 50,000 LP tokens of liquidity are created to create a market in a decentralised exchange, then 5% of these LP tokens (2,500 LP tokens) are sent to the treasury). Sellers may  also be required to pay or burn ETH or FVT in order to submit assets to be sold at the auction.
* |B| **bank.vote**- A small percentage of tokens vested by other projects are sent to the finance.vote treasury (e.g. TOKEN-A vests 1 million tokens in bank.vote and a 0.2% (2,000 tokens) vested allocation is attributed to the finance.vote treasury).
* |I| **influence.vote**- Projects wishing to set up their own voting identity tokens may be required to pay or burn ETH or FVT to do so. Additionally, their users may be required to burn FVT to claim their voting identities.
* |M| **markets.vote** - Projects wishing to set up their own prediction markets or identity systems may be required to pay or burn ETH or FVT to do so.
* |Y| **yield.vote**- A small portion of the LP tokens staked in some liquidity pools is sent to the finance.vote treasury.



Governance and Decentralisation
-----------------------------------
The FVT token began with *Day One Utility*. Token holders were able to gain access to the markets.vote application. The markets.vote application is deployed on decentralised systems, so that the finance.vote team cannot take them down. *The utility of FVT was immediate and irrevocable, and therefore not dependent upon future action by the finance.vote team.*

The locus of control in the finance.vote ecosystem will be continually and progressively transitioned to the token holders in totality over the course of the ecosystem development. Key functionality will be developed along a phased deployment of the network, with the finance.vote team retaining control only for as long as absolutely necessary. The final form of the network will be a decentralised, permissionless system governed by the FVT and identity holders.

.. networks - how do they work separately and how they sync), ropsten, 
    how can there be multiple networks and move tokens between, 
    the bridge?; adding a third network? and bridge to that

Networks
==================
| Networks are Ethereum environments which are used either for actual transactions or for testing purposes, depending on what kind of network is chosen. To begin, a user must first have a virtual wallet. Yes, this applies to developers and testers as well.

| 
| Networks subdivide into two main categories: Public and Private.

Public Networks
----------------
| Public networks can be used by anyone in the world as long as they have an internet connection. Anyone can read and create transactions, and also validate executing ones.  Validation is reached by a consensus of nodes.
| 
| Public networks subdivide into Mainnet and Testnet.

MainNet
***********
MainNet is where actual transactions occur on the distributed ledger. Real exchanges of real tokens are happening here. Within MainNet, any user of any node connected to the network can read all the contract code and data. The only limitation that is implemented on MainNet is the permission check in contract code, which determines which accounts can update the state of a contract.

Testnet
***********
| Testnet, as the name suggests, are networks used to test functionalities and monitor the blockchain network performance. They are public, but currency used in the transactions are valueless. 
|
| This network allows someone to test the functionality of some code that it is working the way it should work, but without any risk of losing money or breaking the main blockchain network. Testnet can be likened to a staging server where software, websites, and services are tested. The possibility of reusing the same test files allows for an accurate comparison between various test runs and thus catching potential errors and network failures. In addition, having tested out a whole project makes MainNet deployments much faster.
|
| Some blockchains even provide testing methodologies, tools, and certifications to accurately test complex networks at scale to help increase productivity and infallibility.
|
| In the case of creating a dApp that integrates with an existing smart contract(s), Testnet becomes a very convenient way of testing since most projects working on MainNet already have copies deployed to testnets. And it's important to remember that testing any contract code before deploying to the MainNet is crucial.
| 
| Types of testnets:

Görli
~~~~~~~~~~~
A proof-of-authority testnet that works across clients.

Kovan
~~~~~~~~~~~
A proof-of-authority testnet for those running OpenEthereum clients.

Rinkeby
~~~~~~~~~~~
A proof-of-authority testnet for running Geth client.

Ropsten
~~~~~~~~~~~
A proof-of-work testnet. This means it's the best like-for-like representation of Ethereum.

Testnet Faucets
~~~~~~~~~~~~~~~~~~~~~~
As mentioned before, currency used in testnets have no real value, so there are no markets for testnet currency. To get some currency for testing purposes, one can retrive some from a testnet network faucet. Most faucets are webapps where after entering a virtual wallet address, a request is made and then test current is sent to the wallet. 

Private Networks
-------------------
Private networks are isolated networks since their nodes are not connected to any public network, neither MainNet nor testnet.

Development Networks
**********************
| Development networks may be described as a local blockchain instance to test a dApp. It's similar to creating a local server on a computer for web development. Thanks to this, iteration may be much faster than it would on a public testnet.
| 
| Within our project we use a dedicated tool to assist with this--Ganache CLI along with Truffle Development Environment.

Consortium Networks
**********************
| Consortium networks are also known as semi-private, federated, or permissioned networks. The consensus process is controlled by a pre-defined set of nodes that are trusted. For example, there could be a consortium of 10 different individual institutions (each of them operating one node), and of those 10 nodes, 6 must sign every block to validate it.
| 
| To better understand, we may think of a public network as the public internet and the consortium network as a private intranet.

Is it possible to send tokens between networks?
------------------------------------------------
**IN BUILD.**

.. *"Yes.*:
    *In Layman terms, a Cross-chain Technology is an emerging technology that seeks to allow transmission of value and information between different blockchain networks.*
    *blockchain interoperability*
    *Cross-chain, protocol ensures interoperability between blockchains, thus enabling the exchange of value as well as information between various networks. *
    *Cross blockchain compatibility, allows different blockchains to communicate with one another without the help of intermediaries. What this means is that blockchains sharing similar networks will* *be able to transfer value between each other.
    *companies will be able to transact with clients from other compatible blockchains. The entire process will take place without any downtime or expensive transaction fees.*
    *Interoperability is essentially the ability to see and access information across various blockchain systems. For example, should a person send data to another blockchain, the recipient should be* *able to read it, comprehend and react with little effort? However, that is not possible, at the moment, as it is impossible to share information between says Bitcoin and Ethereum blockchains.*
    *Cross Chain, technology seeks to solve all this by enhancing interoperability between blockchains. Emerging projects are slowly buying the idea as they attempt to come up with platforms that can*
    *communicate with one another without the need of a third party.*
    *Given the ever-growing need to enhance the connection between various blockchain networks, many developers are already working on optimal solutions. The number of blockchain interoperability* *projects is on the rise as a result as developers look to accelerate blockchain mass adoption."*


How do networks sync?
------------------------
**IN BUILD.**

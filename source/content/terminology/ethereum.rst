Ethereum is a blockchain platform that allows users to upload and execute computer programs known as Smart Contracts. Ethereum Smart Contracts
can be written in a variety of Turing Complete languages,
the most popular being Solidity [9]. Source code is compiled
into a bytecode representation. The bytecode can then be
deployed using a contract creation transaction. Contracts have
a special constructor function that only runs when the contract
creation transaction is being processed. This function is used
to initialize memory and call other contract code. Miners
execute the bytecode inside the Ethereum Virtual Machine
(EVM). At present, each miner must execute all transactions
for all contracts and hold the current value of all the memory
associated with all of the contracts. 


Ethereum [8] to platforma blockchain, która umożliwia użytkownikom przesyłanie i uruchamianie programów komputerowych znanych jako inteligentne kontrakty. Inteligentne kontrakty Ethereum
można napisać w różnych językach Turing Complete,
najpopularniejszym jest Solidity [9]. Kod źródłowy jest skompilowany
na reprezentację kodu bajtowego. Kod bajtowy może wtedy być
wdrożone przy użyciu transakcji tworzenia kontraktu. Kontrakty mają
specjalna funkcja konstruktora, która działa tylko wtedy, gdy kontrakt
transakcja tworzenia jest przetwarzana. Ta funkcja jest używana
do inicjalizacji pamięci i wywołania innego kodu kontraktu. Górniczy
wykonaj kod bajtowy w wirtualnej maszynie Ethereum
(EWM). Obecnie każdy górnik musi wykonać wszystkie transakcje
dla wszystkich kontraktów i zachowaj aktualną wartość całej pamięci
związane ze wszystkimi umowami.

Ethereum
============
Ethereum is a blockchain platform where registered users are able to upload and execute computer programs called Smart Contracts. 
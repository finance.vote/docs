small controllers that you get the functions from to be able to use "backend" stuff; 


Ethereum is a blockchain platform that allows users to upload and execute computer programs known as Smart Contracts. Ethereum Smart Contracts
can be written in a variety of Turing Complete languages,
the most popular being Solidity [9]. Source code is compiled
into a bytecode representation. The bytecode can then be
deployed using a contract creation transaction. Contracts have
a special constructor function that only runs when the contract
creation transaction is being processed. This function is used
to initialize memory and call other contract code. Miners
execute the bytecode inside the Ethereum Virtual Machine
(EVM). At present, each miner must execute all transactions
for all contracts and hold the current value of all the memory
associated with all of the contracts. 


When deployed to a blockchain, a smart contract is a set of instructions that can be executed without intervention from third parties. The code of a smart contract determines how it responds to input, just like the code of any other computer program.

A valuable feature of smart contracts is that they can store and manage on-chain assets (like ETH or ERC20 tokens), just like you or I can with an Ethereum wallet. Because they have an on-chain address, like a wallet, they can do everything any other address can. This opens the door for programming automated actions when receiving and transferring assets.
Comparing Vote Markets on Binance Smart Chain and Ethereum
============================================================
| November 2021

----

| **An analysis by @LIzzl Datadeo#8507**. For original analytics see :download:`this pdf file <../../images/analytics/FV_Vote-Markets-BSC_vs_ETH.pdf>`.
| Original graphs can be seen `here <https://dune.xyz/Lizzl/Finance.vote_Vote-Markets>`_.
| Raw data: `github.com <https://github.com/datadeo/market.vote_analysis/blob/main/README.md>`_.

----
| Since April 2021 to November 2021, users have voted 30 rounds on the Binance Smart Chain to predict the market performance of Defi tokens using a new voting mechanism called Quadratic Voting.
|
| This report analyses the information that was compiled from those votes and compares them with the data of 30 rounds of voting on Ethereum. 
|
| The main questions answered are:

    - How did users interact with the platform?
    - How did voters apply Quadratic Voting?
    - What makes a user a successful predictor?
    - What is the predictive power of markets.vote?


What is Quadratic Voting?
--------------------------
| That's a voting strategy that helps balance the voting power of big players and small ones. The idea standing behind QV mechanism is to make each additional vote cost a lot more than the previous one. 
| It's worth to remember that in QV participants vote for or against an issue, but also express how strongly they feel about it.
| The table down below depicts how cost in tokens depends on the number of votes:

================== ================== 
  Number of votes   Cost in Tokens
================== ================== 
    1                  1
    2                  4
    3                  9
    4                  16
    5                  25
    6                  36
    7                  49
    8                  64
    9                  81
    10                 100
================== ================== 

| More on quadratic voting: `click here <https://financevote.readthedocs.io/en/latest/content/whitepaper/19_token_economics.html#quadratic-voting>`_ or `here <https://financevote.readthedocs.io/en/latest/content/whitepaper/14_vote_market.html#quadratic-voting>`_.

How does the markets.vote work?
--------------------------------
| Users are incentivized to make market predictions in a series of tournaments focussed on a basket of crypto assets.
|
| Quadratic voting is used to generate a consensus in a perceived future market order. Users get a default voting power of 100$V. They then spend a budget of voting power to create a new order, based on their perception of token quality and future potential market performance.
|
| Users are rewarded with a proportional share of a network-generated reward pool depending on the proportionality of their correctness.
|
| Users can amplify their voting power beyond the starting level by demonstrating a history of correct decision-making in the markets, or by purchasing more identities.
|
| More on markets.vote in `the whitepaper <https://financevote.readthedocs.io/en/latest/content/whitepaper/14_vote_market.html>`_. Check out also `markets.vote app <https://marketsdotvote.eth.link/#/vote/how-it-works>`_.

Voter Activity
---------------

+------------------------+-----------------+-----------------+
| Chain                  | BSC             | ETH             |
+========================+=================+=================+
| Total voters           | 151             | 248             |
+------------------------+-----------------+-----------------+
| Total votes            | 7994            | 840             |
+------------------------+-----------------+-----------------+
| Total voterIDs         | 654             | 318             |
+------------------------+-----------------+-----------------+
| Average votes per round| 265             | 28              |
+------------------------+-----------------+-----------------+
| Average user would mint| 4 voterIDs      | 2 voterIDs      |
+------------------------+-----------------+-----------------+

| For up to date statistics visit `Lizzl page on Dune Analytics <https://dune.xyz/Lizzl/Finance.vote_Vote-Markets>`_.

Adoption Curve
^^^^^^^^^^^^^^
ETH
****

.. raw:: html

    <iframe id="ETHadoption"
        title="ETHadoption"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/211090/396383/a4f2bbd7-e77c-4f08-87be-598ccec9eef5">
    </iframe>

|

BSC
****

.. raw:: html

    <iframe id="BSCadoption"
        title="BSCadoption"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/209061/392154/b9e38e72-3a09-471f-926f-8be2acd5a0ff">
    </iframe>

|

Voter Turnout
^^^^^^^^^^^^^^
ETH
******

.. raw:: html

    <iframe id="ETHturnout"
        title="ETHturnout"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/211099/396410/549f1508-4fba-4d5b-9654-8938cccf93f6">
    </iframe>

|

BSC
****

.. raw:: html

    <iframe id="BSCturnout"
        title="BSCturnout"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/200999/375115/72b257bb-fad5-4f2f-a13d-de070564c2ae">
    </iframe>

|

VoterIDs per Wallet
^^^^^^^^^^^^^^^^^^^^
ETH
****

.. raw:: html

    <iframe id="ETHwalletIDs"
        title="ETHwalletIDs"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/212234/398687/037358a0-87d6-4bc5-95ff-3d8fb3008ce4">
    </iframe>

|
BSC
****

.. raw:: html

    <iframe id="BSCwalletIDs"
        title="BSCwalletIDs"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/212059/398316/23c71749-ea02-4714-a3c0-c49d69a75b13">
    </iframe>

|

Distribution of Voting Power and VoterIDs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
ETH
****

.. image:: ../../images/analytics/distributionETH.jpg

|
BSC
****

.. image:: ../../images/analytics/distributionBSC.jpg


How did voters apply QV?
-------------------------
ETH
^^^^

.. |ETHhand| image:: ../../images/analytics/handsETH.png
    :width: 300px

.. |BSChand| image:: ../../images/analytics/handsBSC.png
    :width: 300px

.. |ETHcontr| image:: ../../images/analytics/contributionETH.jpg

.. |BSCcontr| image:: ../../images/analytics/contributionBSC.jpg


.. container:: box
    
    .. container:: left
        
        |ETHhand|   
        
    .. container:: right
        
        On average, voters used **58%** of their voting power per vote on ETH. 
        
        **100, 200 & 99** were the three most used amounts of voting power.

        On average, voters made **2** choices per vote on ETH:
        With **10, 1, 2** being the most used coin choices. 

|ETHcontr|
        

        


BSC
^^^^

.. container:: box
    
    .. container:: left
        
        |BSChand|   
        
    .. container:: right
        
        On average, voters used **40%** of their voting power per vote on BSC. 
        
        **100, 99 & 97** were the three most used amounts of voting power.

        On average, voters made **10** choices per vote on BSC, choosing all ten coins in one vote. 
    
|BSCcontr|

Coin clairvoyants
-----------------

+------------------------------------------------------------------------------------------+
| Top 3 wallets with most Voting Power on BSC chain                                        |
+==============+=======================+=======================+===========================+
| Address      | 0x34...39EA           | 0xE3...D58f           | 0x8E...734b               |
+--------------+-----------------------+-----------------------+---------------------------+
| Voting Power | 45 515                | 36 498                | 30 994                    |
+--------------+-----------------------+-----------------------+---------------------------+
| # of voterID | 50                    | 32                    | 27                        |
+--------------+-----------------------+-----------------------+---------------------------+
| # of rounds  | 30                    | 28                    | 26                        |
| participated |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| % of voting  | 36.9%                 | 33.3%                 | 29.9%                     |
| power used   |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| AVG # of     | 9.6                   | 9.6                   | 9.8                       |
| choices      |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| 3 most weight| [3,3,4,1,4,4,2,2,3,4] | [3,3,3,2,4,3,3,3,3,4] | [5,5,5,5,5,5,3,4,3,4]     |
|              +-----------------------+-----------------------+---------------------------+
| combination  | [7,7,7,8,8,6,5,6,7]   | [4,3,3,2,3,3,3,3,4,3] | [10,8,10,7,10,10,8,8,8,8] |
|              +-----------------------+-----------------------+---------------------------+
| used         | [9,9,9,9,9,9,3,9,9]   | [4,4,5,5,4,4,3,6,5,4] | [4,4,4,5,4,4,3,3,3,3]     |
+--------------+-----------------------+-----------------------+---------------------------+



+------------------------------------------------------------------------------------------+
| Top 3 wallets with most Voting Power on ETH chain                                        |
+==============+=======================+=======================+===========================+
| Address      | 0x34...39EA           | 0x6e...dDec           | 0x67...e640               |
+--------------+-----------------------+-----------------------+---------------------------+
| Voting Power | 5400                  | 4406                  | 3501                      |
+--------------+-----------------------+-----------------------+---------------------------+
| # of voterID | 50                    | 5                     | 15                        |
+--------------+-----------------------+-----------------------+---------------------------+
| # of rounds  | 3                     | 26                    | 16                        |
| participated |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| % of voting  | 95.9%                 | 45.9%                 | 61.3%                     |
| power used   |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| AVG # of     | 4.1                   | 3.7                   | 9.9                       |
| choices      |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| 3 most weight| [5,5,5,5]             | [10,10]               | [4,4,5,4,4,4,4,4,4,4]     |
|              +-----------------------+-----------------------+---------------------------+
| combination  | [5,5,5,5,5]           | [12,12]               | [4,4,4,4,4,4,4,4,4,4]     |
|              +-----------------------+-----------------------+---------------------------+
| used         |                       | [10]                  | [3,3,4,3,3,3,3,3,3,3]     |
+--------------+-----------------------+-----------------------+---------------------------+

----

+------------------------------------------------------+
| Top 3 Voting Power Earners on BSC                    |
+===============+============+============+============+
| Address       | 0x34...39EA| 0xE3...D58f| 0x8E...734b|
+---------------+------------+------------+------------+
| Bought Voting | 5000       | 3200       | 2700       |
|               +            +            +            +
| Power         |            |            |            |
+---------------+------------+------------+------------+
| Earned Voting | 40 515     | 33 298     | 28 294     |
|               +            +            +            +
| Power         |            |            |            |
+---------------+------------+------------+------------+
| % Increase    | 8.1%       | 10.4%      | 10.4%      |
+---------------+------------+------------+------------+


+------------------------------------------------------------------------------------------+
|  On BSC the Top 3 Earners are the same wallets with most Voting Power                    |
+==============+=======================+=======================+===========================+
| # of rounds  | 30                    | 28                    | 26                        |
| participated |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| % of voting  | 36.9%                 | 33.3%                 | 29.9%                     |
| power used   |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| AVG # of     | 9.6                   | 9.6                   | 9.8                       |
| choices      |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| 3 most weight| [3,3,4,1,4,4,2,2,3,4] | [3,3,3,2,4,3,3,3,3,4] | [5,5,5,5,5,5,3,4,3,4]     |
|              +-----------------------+-----------------------+---------------------------+
| combination  | [7,7,7,8,8,6,5,6,7]   | [4,3,3,2,3,3,3,3,4,3] | [10,8,10,7,10,10,8,8,8,8] |
|              +-----------------------+-----------------------+---------------------------+
| used         | [9,9,9,9,9,9,3,9,9]   | [4,4,5,5,4,4,3,6,5,4] | [4,4,4,5,4,4,3,3,3,3]     |
+--------------+-----------------------+-----------------------+---------------------------+

----

+------------------------------------------------------+
| Top 3 Voting Power Earners on ETH                    |
+===============+============+============+============+
| Address       | 0x6e...dDec| 0xDc...CA7D| 0xB3...6B7C|
+---------------+------------+------------+------------+
| Bought Voting | 500        | 200        | 100        |
|               +            +            +            +
| Power         |            |            |            |
+---------------+------------+------------+------------+
| Earned Voting | 3906       | 1074       | 469        |
|               +            +            +            +
| Power         |            |            |            |
+---------------+------------+------------+------------+
| % Increase    | 781.2%     | 537%       | 375%       |
+---------------+------------+------------+------------+

+------------------------------------------------------------------------------------------+
|  How did the most successful voters used the vote markets?                               |
+==============+=======================+=======================+===========================+
| # of rounds  | 26                    | 25                    | 20                        |
| participated |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| % of voting  | 45.9%                 | 47.5%                 | 45.3%                     |
| power used   |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| AVG # of     | 3.7                   | 5.8                   | 9.7                       |
| choices      |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| 3 most weight| [10,10]               | [5,5,5,5]             |                           |
|              +-----------------------+-----------------------+                           +
| combination  | [12,12]               | [+48 DIFFERENT        | [EACH VOTE A DIFFERENT    |
|              +-----------------------+                       +                           +
| used         | [10]                  | COMBINATIONS]         | COMBINATION]              |
+--------------+-----------------------+-----------------------+---------------------------+

----

+------------------------------------------------------------+
| Top 3 VoterIDs BSC                                         |
+===============+============+============+==================+
| VoterID       | 54         | 61         | 121              |
+---------------+------------+------------+------------------+
| Voting Power  | 3475       | 2903       | 2628             |
+---------------+------------+------------+------------------+

+-----------------------------------------------------------------+
| How did the most successful VoterIDs used the vote markets?     |
+==============+================+================+================+
| # of rounds  | 25             | 22             | 27             |
| participated |                |                |                |
+--------------+----------------+----------------+----------------+
| % of voting  | 21.5%          | 20.6%          | 26.9%          |
| power used   |                |                |                |
+--------------+----------------+----------------+----------------+
| AVG # of     | 4.3            | 4              | 9.3            |
| choices      |                |                |                |
+--------------+----------------+----------------+----------------+
| 3 most weight| [EACH VOTE     | [EACH VOTE     | [EACH VOTE     |
+              +                +                +                +
| combination  | A DIFFERENT    | A DIFFERENT    | A DIFFERENT    |
+              +                +                +                +
| used         | COMBINATION]   | COMBINATION]   | COMBINATION]   |
+--------------+----------------+----------------+----------------+

----

+------------------------------------------------------------+
| Top 3 VoterIDs ETH                                         |
+===============+============+============+==================+
| VoterID       | 195        | 1          | 197              |
+---------------+------------+------------+------------------+
| Voting Power  | 2021       | 1226       | 1056             |
+---------------+------------+------------+------------------+

+-----------------------------------------------------------------+
| How did the most successful VoterIDs used the vote markets?     |
+==============+================+================+================+
| # of rounds  | 23             | 27             | 23             |
| participated |                |                |                |
+--------------+----------------+----------------+----------------+
| % of voting  | 39%            | 28.4%          | 42.7%          |
| power used   |                |                |                |
+--------------+----------------+----------------+----------------+
| AVG # of     | 3.6            | 7.6            | 3.6            |
| choices      |                |                |                |
+--------------+----------------+----------------+----------------+
| 3 most weight| [EACH VOTE     | [EACH VOTE     | [EACH VOTE     |
+              +                +                +                +
| combination  | A DIFFERENT    | A DIFFERENT    | A DIFFERENT    |
+              +                +                +                +
| used         | COMBINATION]   | COMBINATION]   | COMBINATION]   |
+--------------+----------------+----------------+----------------+

Market predictions
------------------
| **21** out of **30** coins were predicted successfully on **BSC**. 
| Successful predictions were made in all rounds but **3,5,6,10,12,14,16,23,29**.
| **BUSD** was the token that was predicted most successfully.
|
| **6** out of **30** coins were predicted successfully on **ETH**.
| Successful predictions were made in rounds **3,7,12,14,24,28**.
| There was no particular coin that was predicted more successfully than others.


.. |BSCgraph| image:: ../../images/analytics/BSCgraph.png
    :width: 45%

.. |ETHgraph| image:: ../../images/analytics/ETHgraph.png
    :width: 45%

.. container:: box
    
    .. container:: left
        
        BSC
        |BSCgraph|   
        
    .. container:: right
        
        ETH
        |ETHgraph| 
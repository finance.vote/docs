================
Token Economics
================
Design Philosophy
=================
Decentralisation
------------------
| The finance.vote network is aiming for maximal decentralisation.
|
| That is, we will aim to be as decentralised as possible throughout the development trajectory. Necessarily, the network will launch with a pseudo-autocratic power structure, but this will progress towards decentralized direct democracy over time.
|
| From the outset, founders will manage some keys that hold final decision making power for a period of time. These keys will be transferred to the crowd once an effective governance structure has emerged.
|
| The Finance Vote Token ($FVT) makes this possible, it represents power in the system. The finance.vote ecosystem will pioneer a number of new voting systems that are designed for generating the progressive diffusion of power away from any central arbiter within the system.

Incentivised Action
--------------------
| The $FVT token model is designed to generate user action through rational financial decision making. Users will vote in the system if it makes sense for them to do so. In the world of crypto economics that means it makes financial sense.
|
| All token systems must wrestle with the balance of distributing tokens to participants in order to incentivise adoption and network value dilution arriving from inflation. Adoption comes at a cost.
|
| We balance the incentive dynamics in such a way that they generate adoption through a range of vectors targeted at different stakeholders, including market analysts, decision makers, workers and liquidity providers.
|
| All of these add new tokens into the ecosystem from a starting point, but are distributed to users who bring value to the system. Ultimately, it is the responsibility of the network to balance these incentives against one another, managing the inflationary and deflationary dynamics of the system along with a range of other monetary policy decisions.
|
| The inflationary dynamics of the system at this point are a direction that can be steered by the $FVT holders.

Responsive Governance
----------------------
| Good governance is responsive. It is a system of decision making that responds to the needs and desires of the participants effectively.
|
| The system is designed in such a way that users can build an understanding of key parameters in the system that can be tuned or optimised for maximum engagement and healthy ecosystem growth.
|
| Finance.vote will release a range of voting mechanics that build the reputation of stakeholders in the system. User voting power will be scaled through a mix of meritocratic validation of decision making history and token stake.
|
| Those that have the power in the system will be those that have earned it through participation and high quality decision making.
|
| Those users who are effective in other areas of the system will graduate to the main DAO, the DMF, which will make high level monetary policy decisions and decide how the treasury is spent.

Discourse
----------------------
| The key to good decision making is dialogue.
|
| As the system develops we will find mechanisms to channel and focus discourse into decision making. Our second layer governance system will provide a space for crowd curated user dialogue that can tangibly influence not only our system but others too.
|
| It will be a platform for content aggregation, curation and collective learning. As the ecosystem develops we will build an inclusive international community, which aims to maximise understandability of the system and optimise for involvement in governance decisions.

Utility
----------------------
| The Finance Vote Token will aggregate utility over time by integrating functionality determined by the needs of the system and the desires of the token holders.
|
| The system will hold day one utility. From the moment the token is tradable it will be exchangable for a voting identity in the system and usable within our vote markets. None of the systems functionality will be accessible without an identity.
|
| There are three core aspects of utility that the founders are committing to deploying during the bootstrapping phase of the network: vote markets, second layer governance and social trading. We believe this utility set is strong enough to build a sustainable network, however this token economics system provides a substantial treasury which is to be spent on funding open innovation that falls within the emerging shared design philosophy of the system.

Identity Tokens
----------------------
| Identity is a crucial component in voting systems. The approach used by finance.vote is to issue a decentralised identity token (DIT) to participants.
|
| These will take the form of NFTs that are tradable if the user desires. The cost of an identity token is dynamic (depending on demand) but begins at 100 $FVT. Users must burn this amount in our identity distribution system to obtain a DIT.
|
| This identity is linked to their voting history and their performance within the system. This will generate both a rank and a vote power budget $V, our internal cryptocurrency. They are pseudo anonymous but are a vector for building trust and reputation to participating accounts in the system.

Quadratic Voting
----------------------
| Quadratic voting will change the world. It broadens the parameter space on decision making and is an ideal mechanism for filtering preference. Finance.vote will utilise this framework to build it’s governance system and seek to find effective mechanisms for channeling user action into productive network outcomes.
|
| We utilise quadratic voting in a mechanism we call Semantic Ballot Voting. Users are provided a constantly replenishing budget of vote power ($V), which they distribute on votes through various mechanisms in the system. Typically this action will involve sorting some list of semantic items (token cash tags in the first instance) by preference, through distributing $V quadratically.

Education
----------------------
| A crucial component of the finance.vote ecosystem will be building understanding across our community members of the kinds of systems we are making decisions about. Good decisions arise through informed consensus.
|
| Asymptotically, the best players in this game will be those who can read code. We will support the development of understanding through teaching and materials of how best to understand the cryptospace. The best researchers will win.